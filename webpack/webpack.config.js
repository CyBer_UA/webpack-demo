const path = require("path");
const webpack = require("webpack");
module.exports = {
    entry: {
        index3: "./index3.js"
    },
    context: __dirname,
    output: {
        filename: "dist/[name].js"
    },
    resolve: {
	symlinks: false,
	descriptionFiles: ["package.json"],
    	mainFields: ["main"],
    	extensions: [".js"]
    }
};
